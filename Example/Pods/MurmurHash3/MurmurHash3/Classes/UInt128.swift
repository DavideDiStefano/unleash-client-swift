//
//  UInt128.swift
//  MurmurHash3
//
//  Created by Denis Dzyubenko on 17/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import Foundation

/// A 128-bit unsigned integer type.
public struct UInt128: Equatable {
  /// High portion of the 128-bit value
  public let h1: UInt64
  /// Low portion of the 128-bit value
  public let h2: UInt64

  public init(_ h1: UInt64, _ h2: UInt64) {
    self.h1 = h1
    self.h2 = h2
  }

  /// Returns the 4 32-bit unsigned integers representing this 128-bit value
  public var quad: (UInt32, UInt32, UInt32, UInt32) {
    let h1_lo: UInt32 = UInt32(h1 & 0x00000000ffffffff)
    let h1_hi: UInt32 = UInt32(h1 >> 32)

    let h2_lo: UInt32 = UInt32(h2 & 0x00000000ffffffff)
    let h2_hi: UInt32 = UInt32(h2 >> 32)

    return (h1_hi, h1_lo, h2_hi, h2_lo)
  }

  /// Returns this 128-bit integer type as a string in a hexadecimal format.
  public var hexString: String {
    let parts = self.quad
    return String(format: "%08x%08x%08x%08x", parts.0, parts.1, parts.2, parts.3)
  }
}
