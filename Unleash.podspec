#
# Be sure to run `pod lib lint Unleash.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Unleash'
  s.version          = '0.2.0'
  s.summary          = 'Unleash feature toggle client for Swift.'

  s.description      = <<-DESC
Swift client for Unleash feature toggle service.
                       DESC

  s.homepage         = 'https://gitlab.com/shadone/unleash-client-swift'
  s.license          = { :type => 'Apache', :file => 'LICENSE' }
  s.author           = { 'Denis Dzyubenko' => 'denis@ddenis.info' }
  s.source           = { :git => 'https://gitlab.com/shadone/unleash-client-swift', :tag => s.version.to_s }
  s.swift_version    = '5.0'

  s.ios.deployment_target = '10.0'

  s.source_files = 'Unleash/Classes/**/*'

  s.dependency 'MurmurHash3', '~> 0.2.0'
end
