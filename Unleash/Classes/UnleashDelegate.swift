//
//  UnleashDelegate.swift
//  unleash-test
//
//  Created by Denis Dzyubenko on 13/07/2018.
//  Copyright © 2018 Schibsted. All rights reserved.
//

import Foundation

public protocol UnleashDelegate: class {
  func unleashDidConnect(_ unleash: Unleash)
  func unleash(_ unleash: Unleash, didFailWithError error: Error)
}
