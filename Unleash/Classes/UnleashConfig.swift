//
//  UnleashConfig.swift
//  Unleash
//
//  Created by Denis Dzyubenko on 13/07/2018.
//  Copyright © 2018 Schibsted ASA. All rights reserved.
//

import Foundation

extension Unleash {
  public class Config {
    let appName: String
    let instanceId: String
    let url: String
    let refreshInterval: TimeInterval
    let metricInterval: TimeInterval
    let disableMetrics: Bool
    let strategies: [Strategy]
    let customHeaders: [String:String]

    public init(appName: String,
                instanceId: String? = nil,
                url: String,
                refreshInterval: TimeInterval = 15,
                metricInterval: TimeInterval = 60,
                disableMetrics: Bool = false,
                strategies: [Strategy] = [],
                customHeaders: [String:String] = [:]) {
      self.appName = appName
      self.instanceId = instanceId ?? Config.randomInstanceId()
      self.url = url
      self.refreshInterval = refreshInterval
      self.metricInterval = metricInterval
      self.disableMetrics = disableMetrics
      self.strategies = Config.DefaultStrategies + strategies
      self.customHeaders = customHeaders
    }

    static private func randomInstanceId() -> String {
      // TODO: bundle id + random?
      return "\(arc4random())"
    }

    private static let DefaultStrategies: [Strategy] = [
      DefaultStrategy(),
      UserWithIdStrategy(),
      GradualRolloutRandomStrategy(),
      GradualRolloutSessionIdStrategy(),
      GradualRolloutUserIdStrategy(),
      RemoteAddressStrategy(),
    ]
  }
}
