//
//  APIClient.swift
//  unleash-test
//
//  Created by Denis Dzyubenko on 13/07/2018.
//  Copyright © 2018 Schibsted. All rights reserved.
//

import Foundation

struct StrategyDefinition: Decodable {
  var name: String
  var parameters: [String:ParameterValue]?
}

struct FeatureDefinition: Decodable {
  var name: String
  var description: String?
  var enabled: Bool
  var strategies: [StrategyDefinition] = []
}

protocol APIClient {
  init(baseUrl: String,
       customHeaders: [String:String],
       timeoutInterval: TimeInterval)

  func register(appName: String,
                instanceId: String,
                strategies: [String],
                started: Date,
                interval: Int,
                completion: @escaping (_ error: Error?)->Void)
  
  func fetch(completion: @escaping (_ features: [FeatureDefinition]?, _ error: Error?)->Void)
}

class APIClientImpl: APIClient {
  struct FeaturesResponseHeader: Codable {
    var version: Int
  }
  struct FeaturesResponse: Decodable {
    var version: Int
    var features: [FeatureDefinition]
  }

  struct RegisterPayload: Codable {
    var appName: String
    var instanceId: String
    var sdkVersion: String
    var strategies: [String]
    var started: Date
    var interval: Int
  }

  let baseUrl: String
  let customHeaders: [String:String]
  let timeoutInterval: TimeInterval

  required init(baseUrl: String,
                customHeaders: [String:String],
                timeoutInterval: TimeInterval) {
    self.baseUrl = baseUrl
    self.customHeaders = customHeaders
    self.timeoutInterval = timeoutInterval
  }

  func register(appName: String,
                instanceId: String,
                strategies: [String],
                started: Date = Date(),
                interval: Int,
                completion: @escaping (_ error: Error?)->Void) {
    guard let url = URL(string: "\(self.baseUrl)/api/client/register") else {
      completion(UnleashError.invalidInput("Invalid base url"))
      return
    }

    let payload = RegisterPayload(
      appName: appName,
      instanceId: instanceId,
      sdkVersion: "0.0.1", // TODO: how to get the framework version?
      strategies: strategies,
      started: started,
      interval: interval)

    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .iso8601
    guard let body = try? encoder.encode(payload) else {
      completion(UnleashError.invalidInput("Failed to make registration payload"))
      return
    }

    var urlRequest = URLRequest(url: url)
    urlRequest.httpMethod = "POST"
    urlRequest.httpBody = body

    self.customHeaders.forEach { (key, value) in
      urlRequest.setValue(value, forHTTPHeaderField: key)
    }

    urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")

    URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
      if let error = error {
        completion(UnleashError.networkError(error))
        return
      }

      guard let httpUrlResponse = response as? HTTPURLResponse else {
        assertionFailure("Is it even possible to get something else but HTTPURLResponse?")
        completion(UnleashError.unexpectedStatusCode("assert", 0))
        return
      }

      guard httpUrlResponse.statusCode == 202 else {
        completion(UnleashError.unexpectedStatusCode("registration", httpUrlResponse.statusCode))
        return
      }

      // Success.
      completion(nil)
    }.resume()
  }

  func fetch(completion: @escaping (_ features: [FeatureDefinition]?, _ error: Error?)->Void) {
    guard let url = URL(string: "\(self.baseUrl)/api/client/features") else {
      completion(nil, UnleashError.invalidInput("Invalid base url"))
      return
    }

    var urlRequest = URLRequest(url: url)
    urlRequest.timeoutInterval = self.timeoutInterval
    self.customHeaders.forEach { (key, value) in
      urlRequest.setValue(value, forHTTPHeaderField: key)
    }

    URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
      if let error = error {
        completion(nil, UnleashError.networkError(error))
        return
      }

      guard let httpUrlResponse = response as? HTTPURLResponse else {
        assertionFailure("Is it even possible to get something else but HTTPURLResponse?")
        completion(nil, UnleashError.unexpectedStatusCode("assert", 0))
        return
      }

      guard httpUrlResponse.statusCode == 200 else {
        completion(nil, UnleashError.unexpectedStatusCode("features", httpUrlResponse.statusCode))
        return
      }

      guard let data = data else { return }

      let decoder = JSONDecoder()
      do {
        let header = try decoder.decode(APIClientImpl.FeaturesResponseHeader.self, from: data)
        guard header.version == 1 else {
          completion(nil, UnleashError.unsupportedApiVersion(header.version))
          return
        }
        let payload = try decoder.decode(APIClientImpl.FeaturesResponse.self, from: data)
        completion(payload.features, nil)
      } catch {
        completion(nil, UnleashError.JSONParseError(error))
      }
      }.resume()
  }
}
